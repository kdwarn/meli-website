---
title: v0.6.0 alpha release
ogTitle: v0.6.0 alpha release
ogImage: /images/posts/v0.6.0.png
author: epilys
date: 2020-07-29 00:00:00
date_iso_8601: 2020-07-29T00:00:00+02:00
bees: What if bees flew the coop?
left:
right:
---

A new alpha version is up for test/review. It's starting to look like a real client! [Get it here](/download.html#install). Some highlights are:

- Support for async IO using the `smol` runtime. `IMAP` backend is now async.
- imap: `add server_password_command`
- add optional SMTP client. Instead of using an external command (I was using `msmtp` for example) meli can connect to an SMTP server by itself. See documentation for `send_mail` option.
- Custom themes

## Online demo

I managed to compile meli to WebAssembly, and made [an online demo](https://meli.delivery/wasm2.html) so that you can get a feel for it without having to download/build it first. It's not perfect, but it works. 

![main screen screenshot](/images/posts/v0.6.0.png)

## Themes
### what
meli comes with two themes, dark (the default) and light. Here's how a theme is defined:
```sh
[terminal.themes.watermelon]
"theme_default" = { fg = "#F50431", bg = "#FFD5FD", attrs = "Default" }
"widgets.list.header" = { fg = "#FFD5FD", bg = "#F50431", attrs = "Bold" }
"mail.listing.attachment_flag" = { fg = "LightSlateGrey", bg = "theme_default", attrs = "theme_default" }
"mail.listing.compact.even" = { fg = "White", bg = "#6CA94A", attrs = "Bold" }
"mail.listing.compact.highlighted" = { fg = "#157241", bg = "#f74b41", attrs = "theme_default" }
...
```
etc.

You can also define color aliases:
```sh
[terminal.themes."hunter2"]
color_aliases= { "Jebediah" = "#b4da55" }
"mail.listing.tag_default" = { fg = "$Jebediah" }
...
```

### where
Custom themes can be defined as lists of key-values in the configuration files:

- `$XDG_CONFIG_HOME/meli/config.toml`
- `$XDG_CONFIG_HOME/meli/themes/*.toml`

The application theme is then defined in the configuration as follows:

```sh
[terminal]
theme = "dark"
```

### how
You can create a theme template with a command line flag:
```sh
meli print-default-theme > ~/.config/meli/themes/new_theme.toml
```
Sample themes can be found in the `samples/` directory in the source repository. Detailed documentation can be found in `meli-themes(5)` which is also available [online](/documentation.html)
_sidenote: I couldn't get mandoc to generate large (i.e. longer than a page) tables. There is a GNU mdoc extension for this but it's not supported. Bummer_

![watermelon theme](/images/posts/c8d041b1f73642bb_watermelon_theme.png)

## Other subcommands
```sh
SUBCOMMANDS:
    create-config          create a sample configuration file with available configuration options. If PATH is not
                           specified, meli will try to create it in $XDG_CONFIG_HOME/meli/config.toml
    test-config            test a configuration file for syntax issues or missing options
    man                    print documentation page and exit (Piping to a pager is recommended.) [aliases: docs]
    help                   Prints this message or the help of the given subcommand(s)
    print-default-theme    print default theme in full to stdout and exit
    print-loaded-themes    print loaded themes in full to stdout and exit
    view                   View mail from input file
```

## Usability improvements
- Set **default filter** globally or per-mailbox e.g. show only unseen e-mail (_virtual mailboxes_ coming soon). This is done on the UI and not in the backend, so specifying a filter won't for example make less IMAP requests. This will be supported in the future.
```sh
[listing]
filter = "not flags:seen" # show only unseen messages globally
# or
[accounts."personal account".mailboxes]
"INBOX" = { listing.filter = "not flags:seen" } # override global settings for this mailbox
```
- Oh, and **override** any setting that makes sense per mailbox or per account. You can for example use different sending mail options for each account, or different shortcuts for some mailboxes.
- **Search** in pagers, and the shortcut panel with a simple [KMP](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm) implementation
- **Autoload** mailbox option - don't load big mailboxes on startup till you request them
- **Create/delete** mailbox, {un,}subscribe mailbox
- Set **default headers**
- Manual check for **new mail** (default key <kbd class="□">F5</kbd>). IMAP checks server, maildir can be configured to run a command (setting `refresh_command`)
- Primitive fs autocomplete for commands, till a proper parser with states is written
- Custom date format strings
- `$NO_COLOR` **disable colors** application-wide conforming to [https://no-color.org](https://no-color.org)
- `libnotmuch` is now loaded **dynamically**. It's included in the default build configuration.
- Display `text/html` attachments if the `text/plain` alternative is empty. (setting: `auto_choose_multipart_alternative`)

## Distribution and building
- **Makefile** has been improved:
```sh
/path/to/meli % make help
For a quick start, build and install locally:
 PREFIX=~/.local make install

Available subcommands:
 - meli (builds meli with optimizations in $CARGO_TARGET_DIR)
 - install (installs binary in $BINDIR and documentation to $MANDIR)
 - uninstall
Secondary subcommands:
 - clean (cleans build artifacts)
 - check-deps (checks dependencies)
 - install-bin (installs binary to $BINDIR)
 - install-doc (installs manpages to $MANDIR)
 - help (prints this information)
 - dist (creates release tarball named meli-0.5.1.tar.gz in this directory)
 - deb-dist (builds debian package in the parent directory)
 - distclean (cleans distribution build artifacts)

ENVIRONMENT variables of interest:
* PREFIX = /usr/local
* MELI_FEATURES = unset
* BINDIR = /usr/local/bin
* MANDIR = /usr/local/share/man
* MANPATH = unset
* output of manpath(1) = /home/epilys/.local/share/man:/usr/share/man:/usr/local/man:/usr/local/share/man
* NO_MAN unset
* NO_COLOR unset
```

### binaries and debian package
- [Debian packaging](https://github.com/meli/meli/releases)
- [Binary distrib](https://github.com/meli/meli/releases)

### available in crates.io

[https://crates.io/crates/meli](https://crates.io/crates/meli)

## What's next

- async JMAP
- [NNTP](https://git.meli.delivery/meli/meli/issues/54), [RSS](https://git.meli.delivery/meli/meli/issues/55) backends
- mail storage sync/conversion. The pieces are there, and must be assembled into a functionality.
- Efficient IMAP offline caching. Right now all mail is fetched everytime you launch the client, which obviously is not desirable. There's work being done on `QRESYNC`, `CONDSTORE`, and lazy fetch of messages when viewing a mailbox.
- More GPG support. Signatures can be verified but that's all that exists now. Encryption will be implemented along with WKD support.
- Custom action macros, for example pipe attachments to a script which is already possible with commands.

### Notes

- there is managesieve REPL binary in the repository. I haven't had the time to put managesieve functionality in the TUI yet.
- I haven't explored yet how to expose the API to users properly. I am thinking of using something like [rhai](https://github.com/jonathandturner/rhai) along with general MessagePack IPC.
- There is no API for UI widgets/graphics yet. I'm not sure how to design this right now.
- There's an old [implementation demo](https://git.meli.delivery/meli/meli/src/branch/master/src/plugins/python3/nntp-backend.py) for a mail backend in python using NNTP. It's probably not working anymore.
- There's an old [implementation demo](https://git.meli.delivery/meli/meli/src/branch/master/src/plugins/python3/ansii-plugin.py) for viewing images in ANSI in python using [https://github.com/stefanhaustein/TerminalImageViewer](https://github.com/stefanhaustein/TerminalImageViewer). It might not work anymore.

Follow me on [mastodon](https://chaos.social/@epilys) for updates. Join #meli on Freenode or with matrix's IRC bridge. I am interested in how you're using meli and fixing any problems you will probably encounter.
