## Build
```sh
FORCE=1 cargo run
```

Check for dead links:
```sh
CHECK=1 cargo run
```

## Run
```
cd _site/
python3 -m http.server 8000
```
