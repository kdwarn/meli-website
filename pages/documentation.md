---
bees: What if bees lived in your socks?
ogDescription: Quick start tutorial and reference manual
---

# Quick start tutorial{#quick-start}

Follow the installation instructions in the [README](https://git.meli.delivery/meli/meli/src/branch/master/README.md).

Launch `meli`{.Nm .shake-little} with no arguments, and you will be asked if you want to create a sample configuration.

The [sample configuration](https://git.meli.delivery/meli/meli/src/branch/master/meli/docs/samples/sample-config.toml) (created by default in `~/.config/meli/config.toml`{.Pa}) includes comments with the basic settings required for setting up accounts allowing you to copy and edit right away. See [meli.conf(5)](#meli.conf) for the available configuration options. 

The main screen when you launch `meli`{.Nm .shake-little} is the INBOX of the first account you have configured.

- With <kbd class="□">J</kbd> / <kbd class="□">K</kbd> you can select the next/previous folder. (To go to a specific one directly, press <kbd class="□">Space</kbd> and type the command `go n`{.Cm} where n is the number of the folder)
- With <kbd class="□">h</kbd> / <kbd class="□">l</kbd> you can select the next/previous account.
- Use the arrow keys to browse entries in the e-mail list of a folder. (Along with <kbd class="□ ▭">Home</kbd> / <kbd class="□  ▭ thin-kbd">End</kbd> / <kbd class="□ ▭">PgUp</kbd> / <kbd class="□ ▭">PgDn</kbd>)
- Use Enter to open an entry and <kbd class="□">i</kbd> to exit it.
- Use <kbd class="□">m</kbd> to start writing new e-mail. (or <kbd class="□">R</kbd> to reply to the entry you're currently viewing)
- Use the arrow keys to edit each field of the e-mail.
- Attachments can be added by pressing <kbd class="□">Space</kbd> and typing the command `add-attachment`{.Cm}&nbsp;`PATH`{.Pa} where `PATH`{.Pa} is the filesystem path of the attachment file.
- Edit the draft body at eny time by pressing <kbd class="□">e</kbd>
- Send your e-mail by pressing <kbd class="□">s</kbd>
- Discard or save your draft by pressing <kbd class="□">Space</kbd> and typing the command `close`{.Cm}

At any time, you can press <kbd class="□">?</kbd> to show a list of all available actions and shortcuts, along with every possible setting and command that your version supports.

Press <kbd class="□">T</kbd> to cycle between tabs or <kbd class="□">Alt</kbd> + <kbd class="□">1</kbd> ... <kbd class="□">9</kbd> to choose.

Press <kbd class="□">q</kbd> to exit.
